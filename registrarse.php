<!DOCTYPE html>
<html lang="es">
<!--=========================================================================================
=============  HEAD ======================================================================--> 
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>--EducApp</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

<!--=========================================================================================
=============  Favicons ==================================================================--> 
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!--  
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  -->
<!--=========================================================================================
=============  Vendor CSS Files ==========================================================--> 
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

<!--=========================================================================================
=============  Template Main CSS File ====================================================--> 
  <link href="assets/css/style.css" rel="stylesheet">

</head>
<!--=============  End HEAD ================================================================= 
==========================================================================================-->
<body>

<!--=========================================================================================
=============  Section Header =============================================================--> 

  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">

      <h1 class="logo mr-auto"><a href="index.html">EducApp</a></h1>

     <nav class="nav-menu d-none d-lg-block">
      <ul>
        <form action="iniciosesion1.php" method="post" >
          <div class="form-row">
            <div class="col-md-6 form-group"> 
              <p>Email</p>
              <input type="email" name="email" class="form-control" id="email"  max="100"
 pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" placeholder="Example@algo.com" data-rule="email" data-msg="Por favor, complete todos los campos"/>
              <div class="validate"></div>
            </div>
            <div class="col-md-6 form-group">
              <p>Contraseña</p>
              <input type="password" class="form-control" name="contrasenia" id="contrasenia" placeholder="Contraseña" data-rule="minlen:4" data-msg="Por favor, complete todos los campos"/>
              <div class="validate"></div>
              <p><a href="recuperacontraseña.php">¿Olvidaste tu contraseña?</a></p>
             </div>
          </div>
       
      </ul>
    </nav>

    <button type="submit"  class="get-started-btn">Iniciar Sesión</button> 
 
    </div>
     </form>
      </ul>
    </nav>

    </div>
  </header>
<!--=============  End section Header ======================================================== 
==========================================================================================-->


<!--=========================================================================================
=============  Section contact ===========================================================--> 
    <section id="contact" class="contact" background="hero-bg.jpg">
     <br><br><br><br> <br><br>
     <center>
      <center>
        <h1><strong>Crea una cuenta</strong></h1>
        <h3>Es rápido y fácil</h3>
      </center><br>
      <div data-aos="fade-up">
      <div class="container" data-aos="fade-in">
        <div class="row mt-4">
            <form action="sesion.php" method="post" >
              <div class="form-row">


<!--INPUT NOMBRES-->  
                <div class="col-md-8 form-group" style="padding-left: 30%">
                  <input type="text" name="nombre" class="form-control" id="name"  pattern="[a-zA-Z]{1,15}" placeholder="&#128100; Nombre" data-rule="minlen:3" data-msg="Complete con su nombre" required="" />
                  <div class="validate"></div>
                </div>

<!--INPUT APELLIDO-->
                <div class="col-md-8 form-group" style="padding-left: 30%">
                  <input type="text" class="form-control" name="apellido" id="apellido"  pattern="[a-zA-Z]{1,15}" placeholder="&#128100; Apellido" data-rule="minlen:3" data-msg="Complete con su apellido" />
                  <div class="validate"></div>
                </div>

<!--INPUT EMAIL -->
              <div class="col-md-8 form-group" style="padding-left: 30%">
                <input type="email" class="form-control" name="email" id="email" max="100"
 pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}" placeholder="Example@algo.com" data-rule="email" data-msg=" Complete con un email válido." />
                <div class="validate"></div>
              </div>

<!--INPUT CONTRASEÑA -->
              <div class="col-md-8 form-group" style="padding-left: 30%">
                <input type="password" class="form-control" name="contrasenia" id="contrasenia" minlength="8" required placeholder="&#128273; Ingrese contraseña (minimo 8 caracteres)" maxlength="15"  data-rule="minlen:8" data-msg="Cree una contraseña" />
                <div class="validate"></div>
              </div>

<!--INPUT REPETIR CONTRASEÑA -->
               <div class="col-md-8 form-group" style="padding-left: 30%">
                <input type="password" class="form-control" name="repetircontrasenia" id="repetircontrasenia" minlength="8" required placeholder="&#128273; Repetir contraseña." maxlength="15"  data-rule="minlen:8" data-msg="Repita la contraseña creada" />
                <div class="validate"></div>
              </div>

<!-- SALIDA POR PANTALLA --><div class="col-md-8 form-group" style="padding-left: 30%">
              <p>Al hacer clic en "Registrarte", aceptas nuestras Condiciones, la Política de datos y la Política de cookies.</p><br>
              <p>¿Ya estás registrado? Haz <a href="index.html">aquí</a>.</p>
            </div>
<!--SUBMIT REGISTRARTE -->   
             <div class="col-md-8 form-group" style="padding-left: 30%">
             <button type="submit"  class="get-started-btn">Registrarse</button>   
              </div> 

<!--AL DAR ENTER ROL EN USUARIO DOCENTE - ESTADO DE CUENTA INACTIVA -->
            </form>
 
      </center>
      </div>
      </div>
    </section>

<!--=============  End section contact ====================================================== 
==========================================================================================-->


<!--=========================================================================================
=============  Section footer ============================================================--> 

  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>EducApp</h3>
            <p>
              Buenos Aires, Argentina. </p>

            <p>  <strong>Email:</strong> educaapargentina@gmail.com
            </p>
          </div>

<div class="col-lg-4 col-md-6 footer-links">
            <h4>Links útiles</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Contactanos</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Preguntas frecuentes</a></li>

            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Suscribite Para Recibir novedades !!</h4>
            <form action="comentario.php" method="post">
              <input type="email" name="text"  max="100"
 pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}"  placeholder="  Email"><input type="submit" value="Suscribirse">
            </form>
          </div>

        </div>
        </div>
      </div>
    </div>

    <div class="container d-md-flex py-4">

      <div class="mr-md-auto text-center text-md-left">
        <div class="copyright">
          &copy; Copyright <strong><span>VamNew</span></strong>. All Rights Reserved
        </div>
        <div class="credits">

          Designed by <a href="">BulacioMPaula</a>
        </div>
      </div>
      <div class="social-links text-center text-md-right pt-3 pt-md-0">
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div>
    </div>
  </footer>

<!--=============  End section Footer ======================================================= 
==========================================================================================-->

  <a href="#" class="back-to-top"><i class="bx bx-up-arrow-alt"></i></a>
  <div id="preloader"></div>
<!--=========================================================================================
=============  Vendor JS Files ============================================================--> 
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>
<!--=========================================================================================
============= Template Main JS File ======================================================--> 
  <script src="assets/js/main.js"></script>

</body>

</html>

<!-- ====================================           CHEQUEADO          ============================================== -->
<!-- ====================================          ¡ ATENCION !        ============================================== -->
<!-- ====================================                              ============================================== -->
<!-- ====================================           INCORPORAR         ============================================== -->
<!-- ====================================          Validaciones        ============================================== -->
<!-- ====================================              PHP             ============================================== -->
<!-- ====================================                              ============================================== -->
<!-- ====================================          ¡ ATENCION !        ============================================== -->
<!-- ====================================                              ============================================== -->
<!-- ====================================          ¡ ATENCION !        ============================================== -->




