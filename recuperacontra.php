<!DOCTYPE html>
<html lang="es">
<!--=========================================================================================
=============  HEAD ======================================================================--> 
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>--EducApp</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

<!--=========================================================================================
=============  Favicons ==================================================================--> 
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!--  
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  -->
<!--=========================================================================================
=============  Vendor CSS Files ==========================================================--> 
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

<!--=========================================================================================
=============  Template Main CSS File ====================================================--> 
  <link href="assets/css/style.css" rel="stylesheet">

</head>
<!--=============  End HEAD ================================================================= 
==========================================================================================-->
<body>

<!--=========================================================================================
=============  Section Header =============================================================--> 

  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">
      <h1 class="logo mr-auto"><a href="index.php">EducApp</a></h1>
     </div>
  </header>
 
<!--=============  End section Header ======================================================== 
==========================================================================================-->


<!--=========================================================================================
=============  Section contact ===========================================================--> 

    <section id="contact" class="contact" background="hero-bg.jpg">
     <br><br><br><br> 
     <center>
      <center>
        <h1><strong>RESTABLECER CONTRASEÑA</strong></h1>
        <h3>Es rápido y seguro</h3>
      </center><br>
      <div data-aos="fade-up">
      <div class="container" data-aos="fade-in">
        <div class="row mt-4">
            <form action="rec2.php" method="post" role="form" class="php-email-form">
              <div class="form-row">

<!--INPUT NUEVA CONTRASEÑA -->
              <div class="col-md-12 form-group">
                <input type="password" class="form-control" name="contrasenia" id="contrasenia" placeholder="Contraseña" data-rule="minlen:8" data-msg="Please enter at least 8 chars of subject" />
                <div class="validate"></div>
              </div>

<!--INPUT REPETIR NUEVA CONTRASEÑA -->
              <div class="col-md-12 form-group">
                <input type="password" class="form-control" name="email" id="email" placeholder="ingrese correo" data-rule="minlen:8" data-msg="Please enter at least 8 chars of subject" />
                <div class="validate"></div>
              </div>


<!--SUBMIT GUARDAR CAMBIOS -->      
              
              <button type="submit">Guardar y continuar</button>
            </form>
 <!-- AL DAR ENTER CAMBIO DE CONTRASEÑA Y REDIRECCIONA A index.html -->
      </center>
      </div>
      </div>
    </section>

<!--=============  End section contact ====================================================== 
==========================================================================================-->


<!--=========================================================================================
=============  Section footer ============================================================--> 

  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>EducApp</h3>
            <p>
              Buenos Aires, Argentina. <br><br>

              <strong>Email:</strong> educaapargentina@gmail.com<br>
            </p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Links útiles</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Inicio</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#"></a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Contactanos</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Preguntas frecuentes</a></li>

            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Otros links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">...</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">...</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">...</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Suscribite</h4>
            <p>Obtené todas las novedades</p>
            <form action="" method="post">
              <input type="email" name="text"><input type="submit" value="Suscribirse">
            </form>
          </div>

        </div>
      </div>
    </div>

    <div class="container d-md-flex py-4">

      <div class="mr-md-auto text-center text-md-left">
        <div class="copyright">
          &copy; Copyright <strong><span>VamNew</span></strong>. All Rights Reserved
        </div>
        <div class="credits">

          Designed by <a href="">BulacioMPaula</a>
        </div>
      </div>
      <div class="social-links text-center text-md-right pt-3 pt-md-0">
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div>
    </div>
  </footer>

<!--=============  End section Footer ======================================================= 
==========================================================================================-->

  <a href="#" class="back-to-top"><i class="bx bx-up-arrow-alt"></i></a>
  <div id="preloader"></div>

<!--=========================================================================================
=============  Vendor JS Files ============================================================--> 
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

<!--=========================================================================================
============= Template Main JS File ======================================================--> 
  <script src="assets/js/main.js"></script>

</body>

</html>

<!-- ====================================           CHEQUEADO          ============================================== -->
<!-- ====================================          ¡ ATENCION !        ============================================== -->
<!-- ====================================                              ============================================== -->
<!-- ====================================           INCORPORAR         ============================================== -->
<!-- ====================================          Validaciones        ============================================== -->
<!-- ====================================              PHP             ============================================== -->
<!-- ====================================                              ============================================== -->
<!-- ====================================          ¡ ATENCION !        ============================================== -->
<!-- ====================================                              ============================================== -->
<!-- ====================================                              ============================================== -->
<!-- ====================================          ¡ ATENCION !        ============================================== -->


