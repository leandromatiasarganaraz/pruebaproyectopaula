<!DOCTYPE html>
<html lang="es">
<!--=========================================================================================
=============  HEAD ======================================================================--> 
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>--EducApp</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

<!--=========================================================================================
=============  Favicons ==================================================================--> 
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!--  
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  -->
<!--=========================================================================================
=============  Vendor CSS Files ==========================================================--> 
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

<!--=========================================================================================
=============  Template Main CSS File ====================================================--> 
  <link href="assets/css/style.css" rel="stylesheet">

</head>
<!--=============  End HEAD ================================================================= 
==========================================================================================-->
<body>

<!--=========================================================================================
=============  Section Header =============================================================--> 

  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">
      <h1 class="logo mr-auto"><a href="index.php">EducApp</a></h1>

          <div class="">
            <form action="" method="post">
              <input type="text" name="buscar"><input type="submit" value="Buscar">
            </form>
          </div>     

<nav class="nav-menu d-none d-lg-block">
        <ul>
          <li></li>
          <li class="active"><a href="sesion.php#why-us">Inicio</a></li>
          <li><a href="sesion.php#features">Crear actividad</a></li>
          <li><a href="courses.html">.</a></li>
          <li><a href="trainers.html">.</a></li>
          <li><a href="events.html">.</a></li>
          <li><a href="pricing.html">Usuarios</a></li>
          <li class="drop-down"><a href="">Mi cuenta</a>
            <ul>
              <li><a href="#">Mis actividades</a></li>
              <li><a href="#">Mis datos</a></li>
              <li><a href="configuracion.php">Configuración</a></li>
              <li><a href="#">Salir</a></li>
            </ul>
          </li>
        
      </nav><!-- .nav-menu -->
    </div>

  </header>
 <!--=============  End section Header ======================================================== 
==========================================================================================-->


<!--=========================================================================================
=============  Section contact ===========================================================--> 
    <section id="contact" class="contact" background="hero-bg.jpg">
     <br><br><br><br> 
     <center>
      <center>
        <h1><strong>Configuración de cuenta</strong></h1>
        <h3>Es rápido y fácil</h3>
      </center><br>
      <div data-aos="fade-up">
      <div class="container" data-aos="fade-in">
        <div class="row mt-4">
            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
              <div class="form-row">


<!--INPUT NOMBRES-->  
                <div class="col-md-6 form-group">
                  <input type="text" name="nombre" class="form-control" id="name" placeholder="Nombres" data-rule="minlen:3" data-msg="" />
                  <div class="validate"></div>
                </div>

<!--INPUT APELLIDO-->
                <div class="col-md-6 form-group">
                  <input type="text" class="form-control" name="apellido" id="apellido" placeholder="Apellido" data-rule="minlen:3" data-msg="" />
                  <div class="validate"></div>
                </div>

<!--SELECT GENERO-->
                <div class="col-md-12 form-group">
                  <select name="tipotelefono" class="form-control" id="tipotelefono" placeholder="-- Seleccionar" />
                    <option value="">Genero</option> 
                    <option value="">Femenino</option> 
                    <option value="" selected>Masculino</option>
                    <option value="" selected>Otro</option>
                    <option value="" selected>Prefiero no decirlo</option>
                  </select> 
                  <div class="validate"></div>
                </div>
<!--INPUT TIPO DNI-->
                <div class="col-md-6 form-group">
                  <input type="text" class="form-control" name="tipodni" id="tipodni" placeholder="Tipo de documento" data-rule="minlen:8" readonly="" />
                  <div class="validate"></div>
                </div>

<!--INPUT DNI-->
                <div class="col-md-6 form-group">
                  <input type="text" class="form-control" name="dni" id="dni" placeholder="Numero de documento" data-rule="minlen:8" readonly="" />
                  <div class="validate"></div>
                </div>

<!--SELECT TELEFONO-->
                <div class="col-md-6 form-group">
                  <input type="text" class="form-control" name="telefono" id="telefono" placeholder="Numero de teléfono" data-rule="minlen:8" data-msg="" />
                  <div class="validate"></div>
                </div>

<!--SELECT TIPO TELEFONO-->
                <div class="col-md-6 form-group">
                  <select name="tipotelefono" class="form-control" id="tipotelefono" placeholder="-- Seleccionar" />
                    <option value="">Tipo de telefono</option> 
                    <option value="">Fijo</option> 
                    <option value="" selected>Celular</option>
                  </select> 
                  <div class="validate"></div>
                </div>

<!--INPUT EMAIL -->
              <div class="col-md-12 form-group">
                <input type="email" class="form-control" name="email" id="email" placeholder="Email" data-rule="email" data-msg="" />
                <div class="validate"></div>
              </div>

<!--INPUT CONTRASEÑA -->
              <div class="col-md-12 form-group">
                <input type="password" class="form-control" name="contrasenia" id="contrasenia" placeholder="Contraseña" data-rule="minlen:8" data-msg="" />
                <div class="validate"></div>
              </div>

<!--INPUT REPETIR CONTRASEÑA -->
              <div class="col-md-12 form-group">
                <input type="password" class="form-control" name="repetircontrasenia" id="repetircontrasenia" placeholder="Repetir contraseña" data-rule="minlen:8" data-msg="" />
                <div class="validate"></div>
              </div>

<!--SUBMIT CANCELAR --> 
              <div class="col-md-6 form-group">            
                <div class="text-md-right"><button type="submit">Cancelar</button></div>
              </div>

<!--SUBMIT GUARDAR CAMBIOS -->      
              <div class="col-md-6 form-group">         
              <div class="text-md-left"><button type="submit">Guardar cambios</button></div>
              </div>
            </form>
 
      </center>
      </div>
      </div>
    </section>

<!--=============  End section contact ====================================================== 
==========================================================================================-->


<!--=========================================================================================
=============  Section footer ============================================================--> 

  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>EducApp</h3>
            <p>Buenos Aires, Argentina.</p>
              <p><strong>Email:</strong> educaapargentina@gmail.com</p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Links útiles</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Inicio</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Crear actividad</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Contactanos</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Preguntas frecuentes</a></li>

            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Otros links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">...</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">...</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">...</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Suscribite</h4>
            <p>Obtené todas las novedades</p>
            <form action="" method="post">
              <input type="email" name="text"><input type="submit" value="Suscribirse">
            </form>
          </div>

        </div>
      </div>
    </div>

    <div class="container d-md-flex py-4">

      <div class="mr-md-auto text-center text-md-left">
        <div class="copyright">
          &copy; Copyright <strong><span>VamNew</span></strong>. All Rights Reserved
        </div>
        <div class="credits">

          Designed by <a href="">BulacioMPaula</a>
        </div>
      </div>
      <div class="social-links text-center text-md-right pt-3 pt-md-0">
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div>
    </div>
  </footer>

<!--=============  End section Footer ======================================================= 
==========================================================================================-->

  <a href="#" class="back-to-top"><i class="bx bx-up-arrow-alt"></i></a>
  <div id="preloader"></div>
<!--=========================================================================================
=============  Vendor JS Files ============================================================--> 
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>
<!--=========================================================================================
============= Template Main JS File ======================================================--> 
  <script src="assets/js/main.js"></script>

</body>

</html>

<!-- ====================================           CHEQUEADO          ============================================== -->
<!-- ====================================          ¡ ATENCION !        ============================================== -->
<!-- ====================================                              ============================================== -->
<!-- ====================================           INCORPORAR         ============================================== -->
<!-- ====================================          Validaciones        ============================================== -->
<!-- ====================================              PHP             ============================================== -->
<!-- ====================================                              ============================================== -->
<!-- ====================================          ¡ ATENCION !        ============================================== -->
<!-- ====================================                              ============================================== -->
<!-- ====================================          ¡ ATENCION !        ============================================== -->

