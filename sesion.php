<?php
require_once('controladores/funciones.php');
//=========================================================================================
//==========  conexion base de datos vamdev_gestionar  ====================================
  $usuario = "root";
  $password ="rootaws";
  $servidor ="localhost";
  $basededatos="vamdev_gestionar";
//=========================================================================================
//==========  Conexion a la base de datos con mysql_connect() =============================
$conexion = new mysqli($servidor,$usuario,$password,$basededatos);
  if ($conexion->connect_errno) {
      echo "Se produjo un error al recuperar los datos del servidor.";
      exit();
  }
//=========================================================================================
//=========  traer por POST datos ingresados registrarse.php ==============================
  else{
     
      $nombre=$_POST['nombre'];
      $apellido=$_POST['apellido'];
      $email=$_POST['email'];
      $contrasenia1=$_POST['contrasenia'];
      $contrasenia2=$_POST['repetircontrasenia'];


//=========================================================================================
//=========  IF permitir ingreso a pagina SOLO USUARIOS ===================================
/*  if(($idrol!="1")($idrol!="2")($idrol!="3")){
    echo   '<script>
    alert("No posee los permisos para ingresar ;)");
    window.history.go(-1);
    </script>';
}
  else{ */
    
//=========================================================================================
//=========  generar TOKEN RANDOM para activacion de cuenta ===============================      
  $rand = rand(10000,100000);
  $activacion= md5($rand);

//=========================================================================================
//=========  validar contraseña ½½ repetir contraseña =====================================     
  if($contrasenia1!=$contrasenia2){

        echo  '<script>
    alert("Las contraseñas NO coinciden. Intente nuevamente.");
    window.history.go(-1);
    </script>';
  }

//=========================================================================================
//=========  encriptar contraseña previamente validada ====================================   
    else{ 
//$desencript=base64_encode($password);
       $claveencript=base64_encode($contrasenia1);
//=========================================================================================       
//========= INSERT por defecto en BD IdRol $idrol=2 | IdUsuarioEstado $iniciar ============
    //$idrol = "2";
      $iniciar="2";
//=========================================================================================
//=========  INSERT datos minimos en tabla USUARIO ======================================== 

$insertar= "INSERT INTO USUARIO(Nombres,Apellido,Email,Contrasena,IdUsuarioEstado,Token) values ('$nombre','$apellido','$email','$claveencript','$iniciar','$activacion')";

//=========================================================================================
//=========  VALIDAR si existen los datos ================================================= 

$verificarcorreo=mysqli_query($conexion,"SELECT * FROM USUARIO WHERE Email='$email'");
if(mysqli_num_rows($verificarcorreo)>0){
  
    echo   '<script>
    alert("El usuario ya se encuentra registrado.");
    window.history.go(-1);
    </script>';

    exit;
}
//=========================================================================================
//=========  consulta de datos ============================================================ 
$resultado=mysqli_query($conexion,$insertar);
if(!$resultado){
   echo  '<script>
    alert("Los datos ingresados NO son correctos.");
    window.history.go(-1);
    </script>';     
}

else{
//=========================================================================================
//=========  enviar codigo de verificaciòn ================================================ 
  enviarcorreo_validation($email,$nombre,$activacion);

//=========================================================================================
//=========  Mostrar pantalla de verificar correo 8 seg =================================== 
  ?>

<!DOCTYPE html>
<html lang="es">
<!--=========================================================================================
=============  HEAD ======================================================================--> 
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>--EducApp</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

<!--=========================================================================================
=============  Favicons ==================================================================--> 
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!--  
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  -->
<!--=========================================================================================
=============  Vendor CSS Files ==========================================================--> 

  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

<!--=========================================================================================
=============  Template Main CSS File ====================================================--> 
  <link href="assets/css/style.css" rel="stylesheet">


</head>
<!--=============  End HEAD ================================================================= 
==========================================================================================-->
<body>
<!--=========================================================================================
=============  Section Header =============================================================--> 

  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">
      <h1 class="logo mr-auto"><a href="index.php">EducApp</a></h1>
     </div>
  </header>
<!--=============  End section Header ======================================================== 
==========================================================================================-->


<!--=========================================================================================
=============  Section About =============================================================--> 
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">
<br><br><br>
        <div class="section-title">     
        <h1><strong>¡Bienvenido a EducApp!</strong></h1>
        <h2><?php echo $nombre ?>,<?php echo $apellido ?></h2>
      </center><br></div>
      <div class="row">

          <div class="col-lg-12 pt-4 pt-lg-0 order-2 order-lg-1 content">
            <h3>Verifica tu dirección de correo electronico.</h3>
            <h4>Puede que el mail tarde unos minutos en llegarte.</h4>
            <p class="font-italic">
              
              <p>¿No has recibido el mail? Haz click <a href="">aquí</a>.</p>
            
          </div>
        </div>

      </div>
    </section>
<!--=============  End section About ======================================================== 
==========================================================================================-->


<!--=========================================================================================
=============  Section footer ============================================================--> 

  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>EducApp</h3>
            <p>
              Buenos Aires, Argentina. <br><br>

              <strong>Email:</strong> educaapargentina@gmail.com<br>
            </p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Links útiles</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Inicio</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#"></a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Contactanos</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Preguntas frecuentes</a></li>

            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Otros links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">...</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">...</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">...</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Suscribite</h4>
            <p>Obtené todas las novedades</p>
            <form action="" method="post">
              <input type="email" name="text"><input type="submit" value="Suscribirse">
            </form>
          </div>

        </div>
      </div>
    </div>

    <div class="container d-md-flex py-4">

      <div class="mr-md-auto text-center text-md-left">
        <div class="copyright">
          &copy; Copyright <strong><span>VamNew</span></strong>. All Rights Reserved
        </div>
        <div class="credits">

          Designed by <a href="">BulacioMPaula</a>
        </div>
      </div>
      <div class="social-links text-center text-md-right pt-3 pt-md-0">
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div>
    </div>
  </footer>
<!--=============  End section Footer ======================================================= 
==========================================================================================-->

  <a href="#" class="back-to-top"><i class="bx bx-up-arrow-alt"></i></a>
  <div id="preloader"></div>
<!--=========================================================================================
=============  Vendor JS Files ============================================================--> 

  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>

  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

<!--=========================================================================================
============= Template Main JS File ======================================================--> 

  <script src="assets/js/main.js"></script>

</body>

</html>

<?php
//=========================================================================================
//=========  Mostrar pantalla de verificar correo 8 seg =================================== 
//=========  Retorno Index.html =========================================================== 
//=========================================================================================

echo "<meta http-equiv=\"refresh\" content=\"8;url=http://educapp.vamdev.com.ar/index.html\"/>";

//=========================================================================================
//=========  Cierran ELSE de IF =========================================================== 
}//if ($conexion->connect_errno) {

//}
}//if($contrasenia1!=$contrasenia2){
}//if(!$resultado){ 

?>
