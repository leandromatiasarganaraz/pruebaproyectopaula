-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 14-07-2020 a las 16:50:53
-- Versión del servidor: 5.7.30-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.24-0ubuntu0.18.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `EducAr`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ACTIVIDAD`
--

CREATE TABLE `ACTIVIDAD` (
  `IdActividad` smallint(5) NOT NULL,
  `IdNivel` smallint(5) DEFAULT NULL,
  `IdMateria` smallint(5) DEFAULT NULL,
  `IdTipoActividad` smallint(5) DEFAULT NULL,
  `IdDetalleActividad` smallint(5) DEFAULT NULL,
  `IdUsuario` smallint(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `AUD_USUARIO`
--

CREATE TABLE `AUD_USUARIO` (
  `IdAudUsuario` smallint(5) NOT NULL,
  `IdUsuario` smallint(5) DEFAULT NULL,
  `NombreAnterior` varchar(50) DEFAULT NULL,
  `NombreNuevo` varchar(50) DEFAULT NULL,
  `ApellidoAnterior` varchar(50) DEFAULT NULL,
  `ApellidoNuevo` varchar(50) DEFAULT NULL,
  `DniAnterior` int(10) DEFAULT NULL,
  `DniNuevo` int(10) DEFAULT NULL,
  `IdTipoDniAnterior` smallint(5) DEFAULT NULL,
  `IdTipoDniNuevo` smallint(5) DEFAULT NULL,
  `IdGeneroAnterior` smallint(5) DEFAULT NULL,
  `IdGeneroNuevo` smallint(5) DEFAULT NULL,
  `EmailAnterior` varchar(100) DEFAULT NULL,
  `EmailNuevo` varchar(100) DEFAULT NULL,
  `ContraseñaAnterior` varchar(50) DEFAULT NULL,
  `ContraseniaAnterior` varchar(50) DEFAULT NULL,
  `ContraseniaNueva` varchar(50) DEFAULT NULL,
  `IdRolAnterior` smallint(5) DEFAULT NULL,
  `IdRolNuevo` smallint(5) DEFAULT NULL,
  `IdUsuarioEstadoAnterior` smallint(5) DEFAULT NULL,
  `IdUsuarioEstadoNuevo` smallint(5) DEFAULT NULL,
  `FechaYHora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `DETALLE_ACTIVIDAD`
--

CREATE TABLE `DETALLE_ACTIVIDAD` (
  `IdDetalleActividad` smallint(5) NOT NULL,
  `Titulo` varchar(100) DEFAULT NULL,
  `Consigna` text,
  `IdPregunta` smallint(5) DEFAULT NULL,
  `IdTipoRespuesta` smallint(5) DEFAULT NULL,
  `IdRespuesta` smallint(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `MATERIA`
--

CREATE TABLE `MATERIA` (
  `IdMateria` smallint(5) NOT NULL,
  `Materia` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `NIVEL`
--

CREATE TABLE `NIVEL` (
  `IdNivel` smallint(5) NOT NULL,
  `Nivel` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PREGUNTA`
--

CREATE TABLE `PREGUNTA` (
  `IdPregunta` smallint(5) NOT NULL,
  `Pregunta` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `RESPUESTA`
--

CREATE TABLE `RESPUESTA` (
  `IdRespuesta` smallint(5) NOT NULL,
  `IdTipoRespuesta` smallint(5) DEFAULT NULL,
  `IdUsuario` smallint(5) DEFAULT NULL,
  `Respuesta` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ROL`
--

CREATE TABLE `ROL` (
  `IdRol` smallint(5) NOT NULL,
  `Rol` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ROL`
--

INSERT INTO `ROL` (`IdRol`, `Rol`) VALUES
(1, 'ROOT'),
(2, 'PROFESOR'),
(3, 'ALUMNO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TIPO_ACTIVIDAD`
--

CREATE TABLE `TIPO_ACTIVIDAD` (
  `IdTipoActividad` smallint(5) NOT NULL,
  `TipoActividad` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TIPO_DNI`
--

CREATE TABLE `TIPO_DNI` (
  `IdTipoDni` smallint(5) NOT NULL,
  `TipoDni` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `TIPO_DNI`
--

INSERT INTO `TIPO_DNI` (`IdTipoDni`, `TipoDni`) VALUES
(1, 'DOCUMENTO UNICO'),
(2, 'LIBRETA CÍVICA'),
(3, 'LIBRETA DE ENROLAMIENTO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TIPO_RESPUESTA`
--

CREATE TABLE `TIPO_RESPUESTA` (
  `IdTipoRespuesta` smallint(5) NOT NULL,
  `TipoRespuesta` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TIPO_TELEFONO`
--

CREATE TABLE `TIPO_TELEFONO` (
  `IdTipoTelefono` smallint(5) NOT NULL,
  `TipoTelefono` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `TIPO_TELEFONO`
--

INSERT INTO `TIPO_TELEFONO` (`IdTipoTelefono`, `TipoTelefono`) VALUES
(1, 'CELULAR'),
(2, 'FIJO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `USUARIO`
--

CREATE TABLE `USUARIO` (
  `IdUsuario` smallint(5) NOT NULL,
  `FotoPerfil` mediumblob,
  `Nombres` varchar(50) DEFAULT NULL,
  `Apellido` varchar(50) DEFAULT NULL,
  `Dni` int(10) DEFAULT NULL,
  `IdTipoDni` smallint(5) DEFAULT NULL,
  `IdGenero` smallint(5) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Contraseña` varchar(50) DEFAULT NULL,
  `Telefono` varchar(15) DEFAULT NULL,
  `IdTipoTelefono` smallint(5) DEFAULT NULL,
  `IdRol` smallint(5) DEFAULT NULL,
  `IdUsuarioEstado` smallint(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `USUARIO`
--

INSERT INTO `USUARIO` (`IdUsuario`, `FotoPerfil`, `Nombres`, `Apellido`, `Dni`, `IdTipoDni`, `IdGenero`, `Email`, `Contraseña`, `Telefono`, `IdTipoTelefono`, `IdRol`, `IdUsuarioEstado`) VALUES
(1, NULL, 'PAULA', 'BULACIO', 38998056, 1, 1, 'EDUCAAPARGENTINA@GMAIL.COM', '906453452HOLA', '1122334455', 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `USUARIO_ESTADO`
--

CREATE TABLE `USUARIO_ESTADO` (
  `IdUsuarioEstado` smallint(5) NOT NULL,
  `UsuarioEstado` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `USUARIO_ESTADO`
--

INSERT INTO `USUARIO_ESTADO` (`IdUsuarioEstado`, `UsuarioEstado`) VALUES
(1, 'ACTIVO'),
(2, 'INACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `USUARIO_GENERO`
--

CREATE TABLE `USUARIO_GENERO` (
  `IdGenero` smallint(5) NOT NULL,
  `Genero` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `USUARIO_GENERO`
--

INSERT INTO `USUARIO_GENERO` (`IdGenero`, `Genero`) VALUES
(1, 'FEMENINO'),
(2, 'MASCULINO'),
(3, 'PREFIERO NO DECIRLO'),
(4, 'OTRO');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ACTIVIDAD`
--
ALTER TABLE `ACTIVIDAD`
  ADD PRIMARY KEY (`IdActividad`),
  ADD KEY `IdNivel` (`IdNivel`,`IdMateria`,`IdTipoActividad`,`IdDetalleActividad`,`IdUsuario`),
  ADD KEY `IdMateria` (`IdMateria`),
  ADD KEY `IdTipoActividad` (`IdTipoActividad`),
  ADD KEY `IdDetalleActividad` (`IdDetalleActividad`),
  ADD KEY `IdUsuario` (`IdUsuario`);

--
-- Indices de la tabla `AUD_USUARIO`
--
ALTER TABLE `AUD_USUARIO`
  ADD PRIMARY KEY (`IdAudUsuario`);

--
-- Indices de la tabla `DETALLE_ACTIVIDAD`
--
ALTER TABLE `DETALLE_ACTIVIDAD`
  ADD PRIMARY KEY (`IdDetalleActividad`),
  ADD KEY `IdPregunta` (`IdPregunta`,`IdTipoRespuesta`,`IdRespuesta`),
  ADD KEY `IdRespuesta` (`IdRespuesta`),
  ADD KEY `IdTipoRespuesta` (`IdTipoRespuesta`);

--
-- Indices de la tabla `MATERIA`
--
ALTER TABLE `MATERIA`
  ADD PRIMARY KEY (`IdMateria`);

--
-- Indices de la tabla `NIVEL`
--
ALTER TABLE `NIVEL`
  ADD PRIMARY KEY (`IdNivel`);

--
-- Indices de la tabla `PREGUNTA`
--
ALTER TABLE `PREGUNTA`
  ADD PRIMARY KEY (`IdPregunta`);

--
-- Indices de la tabla `RESPUESTA`
--
ALTER TABLE `RESPUESTA`
  ADD PRIMARY KEY (`IdRespuesta`),
  ADD KEY `IdTipoRespuesta` (`IdTipoRespuesta`,`IdUsuario`);

--
-- Indices de la tabla `ROL`
--
ALTER TABLE `ROL`
  ADD PRIMARY KEY (`IdRol`);

--
-- Indices de la tabla `TIPO_ACTIVIDAD`
--
ALTER TABLE `TIPO_ACTIVIDAD`
  ADD PRIMARY KEY (`IdTipoActividad`);

--
-- Indices de la tabla `TIPO_DNI`
--
ALTER TABLE `TIPO_DNI`
  ADD PRIMARY KEY (`IdTipoDni`);

--
-- Indices de la tabla `TIPO_RESPUESTA`
--
ALTER TABLE `TIPO_RESPUESTA`
  ADD PRIMARY KEY (`IdTipoRespuesta`);

--
-- Indices de la tabla `TIPO_TELEFONO`
--
ALTER TABLE `TIPO_TELEFONO`
  ADD PRIMARY KEY (`IdTipoTelefono`);

--
-- Indices de la tabla `USUARIO`
--
ALTER TABLE `USUARIO`
  ADD PRIMARY KEY (`IdUsuario`),
  ADD KEY `IdTipoDni` (`IdTipoDni`,`IdGenero`,`IdRol`,`IdUsuarioEstado`),
  ADD KEY `IdGenero` (`IdGenero`),
  ADD KEY `IdTelefono` (`IdTipoTelefono`),
  ADD KEY `IdRol` (`IdRol`),
  ADD KEY `IdUsuarioEstado` (`IdUsuarioEstado`);

--
-- Indices de la tabla `USUARIO_ESTADO`
--
ALTER TABLE `USUARIO_ESTADO`
  ADD PRIMARY KEY (`IdUsuarioEstado`);

--
-- Indices de la tabla `USUARIO_GENERO`
--
ALTER TABLE `USUARIO_GENERO`
  ADD PRIMARY KEY (`IdGenero`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `ACTIVIDAD`
--
ALTER TABLE `ACTIVIDAD`
  MODIFY `IdActividad` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `AUD_USUARIO`
--
ALTER TABLE `AUD_USUARIO`
  MODIFY `IdAudUsuario` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `DETALLE_ACTIVIDAD`
--
ALTER TABLE `DETALLE_ACTIVIDAD`
  MODIFY `IdDetalleActividad` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `NIVEL`
--
ALTER TABLE `NIVEL`
  MODIFY `IdNivel` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `PREGUNTA`
--
ALTER TABLE `PREGUNTA`
  MODIFY `IdPregunta` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `RESPUESTA`
--
ALTER TABLE `RESPUESTA`
  MODIFY `IdRespuesta` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `ROL`
--
ALTER TABLE `ROL`
  MODIFY `IdRol` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `TIPO_ACTIVIDAD`
--
ALTER TABLE `TIPO_ACTIVIDAD`
  MODIFY `IdTipoActividad` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `TIPO_DNI`
--
ALTER TABLE `TIPO_DNI`
  MODIFY `IdTipoDni` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `TIPO_RESPUESTA`
--
ALTER TABLE `TIPO_RESPUESTA`
  MODIFY `IdTipoRespuesta` smallint(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `TIPO_TELEFONO`
--
ALTER TABLE `TIPO_TELEFONO`
  MODIFY `IdTipoTelefono` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `USUARIO`
--
ALTER TABLE `USUARIO`
  MODIFY `IdUsuario` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `USUARIO_ESTADO`
--
ALTER TABLE `USUARIO_ESTADO`
  MODIFY `IdUsuarioEstado` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `USUARIO_GENERO`
--
ALTER TABLE `USUARIO_GENERO`
  MODIFY `IdGenero` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `ACTIVIDAD`
--
ALTER TABLE `ACTIVIDAD`
  ADD CONSTRAINT `ACTIVIDAD_ibfk_1` FOREIGN KEY (`IdNivel`) REFERENCES `NIVEL` (`IdNivel`),
  ADD CONSTRAINT `ACTIVIDAD_ibfk_2` FOREIGN KEY (`IdMateria`) REFERENCES `MATERIA` (`IdMateria`),
  ADD CONSTRAINT `ACTIVIDAD_ibfk_3` FOREIGN KEY (`IdTipoActividad`) REFERENCES `TIPO_ACTIVIDAD` (`IdTipoActividad`),
  ADD CONSTRAINT `ACTIVIDAD_ibfk_4` FOREIGN KEY (`IdDetalleActividad`) REFERENCES `DETALLE_ACTIVIDAD` (`IdDetalleActividad`),
  ADD CONSTRAINT `ACTIVIDAD_ibfk_5` FOREIGN KEY (`IdUsuario`) REFERENCES `USUARIO` (`IdUsuario`);

--
-- Filtros para la tabla `DETALLE_ACTIVIDAD`
--
ALTER TABLE `DETALLE_ACTIVIDAD`
  ADD CONSTRAINT `DETALLE_ACTIVIDAD_ibfk_1` FOREIGN KEY (`IdRespuesta`) REFERENCES `RESPUESTA` (`IdRespuesta`),
  ADD CONSTRAINT `DETALLE_ACTIVIDAD_ibfk_2` FOREIGN KEY (`IdPregunta`) REFERENCES `PREGUNTA` (`IdPregunta`),
  ADD CONSTRAINT `DETALLE_ACTIVIDAD_ibfk_3` FOREIGN KEY (`IdTipoRespuesta`) REFERENCES `TIPO_RESPUESTA` (`IdTipoRespuesta`);

--
-- Filtros para la tabla `TIPO_DNI`
--
ALTER TABLE `TIPO_DNI`
  ADD CONSTRAINT `TIPO_DNI_ibfk_1` FOREIGN KEY (`IdTipoDni`) REFERENCES `USUARIO` (`IdTipoDni`);

--
-- Filtros para la tabla `USUARIO`
--
ALTER TABLE `USUARIO`
  ADD CONSTRAINT `USUARIO_ibfk_1` FOREIGN KEY (`IdTipoDni`) REFERENCES `TIPO_DNI` (`IdTipoDni`),
  ADD CONSTRAINT `USUARIO_ibfk_2` FOREIGN KEY (`IdGenero`) REFERENCES `USUARIO_GENERO` (`IdGenero`),
  ADD CONSTRAINT `USUARIO_ibfk_3` FOREIGN KEY (`IdTipoTelefono`) REFERENCES `TIPO_TELEFONO` (`IdTipoTelefono`),
  ADD CONSTRAINT `USUARIO_ibfk_4` FOREIGN KEY (`IdRol`) REFERENCES `ROL` (`IdRol`),
  ADD CONSTRAINT `USUARIO_ibfk_5` FOREIGN KEY (`IdUsuarioEstado`) REFERENCES `USUARIO_ESTADO` (`IdUsuarioEstado`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
