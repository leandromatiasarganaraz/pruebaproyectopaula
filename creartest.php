<!DOCTYPE html>
<html lang="es">
<!--=========================================================================================
=============  HEAD ======================================================================--> 
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>--EducApp</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

<!--=========================================================================================
=============  Favicons ==================================================================--> 
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!--  
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  -->
<!--=========================================================================================
=============  Vendor CSS Files ==========================================================--> 
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

<!--=========================================================================================
=============  Template Main CSS File ====================================================--> 
  <link href="assets/css/style.css" rel="stylesheet">

</head>
<!--=============  End HEAD ================================================================= 
==========================================================================================-->
<body>

<!--=========================================================================================
=============  Section Header =============================================================--> 

  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">
      <h1 class="logo mr-auto"><a href="">EducApp</a></h1>

          <div class="">
            <form action="" method="post">
              <input type="text" name="buscar"><input type="submit" value="Buscar">
            </form>
          </div>     

<nav class="nav-menu d-none d-lg-block">
        <ul>
          <li></li>
          <li class="active"><a href="sesion.php#why-us">Inicio</a></li>
          <li><a href="sesion.php#features">Crear actividad</a></li>
          <li><a href="">.</a></li>
          <li><a href="">.</a></li>
          <li><a href="">.</a></li>
          <li><a href="">Usuarios</a></li>
          <li class="drop-down"><a href="">Mi cuenta</a>
            <ul>
              <li><a href="#">Mis actividades</a></li>
              <li><a href="#">Mis datos</a></li>
              <li><a href="configuracion.php">Configuración</a></li>
              <li><a href="#">Salir</a></li>
            </ul>
          </li>
        
      </nav><!-- .nav-menu -->
    </div>

  </header>
 <!--=============  End section Header ======================================================== 
==========================================================================================-->


<!--=========================================================================================
=============  Section contact ===========================================================--> 
    <section id="contact" class="contact" background="hero-bg.jpg">
     <br><br><br><br> 
     <center>
      <center>
        <h1><strong>Crear tu actividad</strong></h1>
        <h3>Es rápido y fácil</h3>
      </center><br>
      <div data-aos="fade-up">
      <div class="container" data-aos="fade-in">
        <div class="row mt-4">
            <form action="creartest1.php" method="post" role="form" >
              <div class="form-row">


<!--SELECT nivel educativo-->
                <p>Seleccione nivel educativo</p>
                <div class="col-md-12 form-group">
                  <?php

       echo"<select class=\"form-control \" id=\"nivel\" name=\"nivel\" name=\"seccion\">\n";
       echo"<option>Selecione un Nivel</option>\n";
       include'conexion.php';
       $consulta="SELECT nivel FROM NIVEL";//para igaular datos
       $resultado=mysqli_query($link,$consulta);//mysqli_querry ejecuta la consulta y paso las variables
  
      while($columna = mysqli_fetch_array($resultado))
       {
        $contenido1=$columna[0];
        echo "<option >".$contenido1."</option>";  

          
    } 

  echo"</select>"; 

?>  
     </div>

<!--SELECT TIPO materia-->
      <p>Seleccione materia</p>
      <div class="col-md-12 form-group">
                   <?php

       echo"<select class=\"form-control \" id=\"materia\" name=\"materia\" name=\"seccion\">\n";
       echo"<option>Selecione un Nivel</option>\n";
      include'conexion.php';
       $consulta="SELECT Materia FROM MATERIA";//para igaular datos
       $resultado=mysqli_query($link,$consulta);//mysqli_querry ejecuta la consulta y paso las variables
  
      while($columna = mysqli_fetch_array($resultado))
       {
        $contenido1=$columna[0];
        echo "<option >".$contenido1."</option>";  

          
    } 

  echo"</select>"; 

?>
       </div>


<!--INPUT TITULO-->  
        <p>Título</p>
        <div class="col-md-12 form-group">
          <input type="text" name="titulo" class="form-control" id="titulo" placeholder="Tìtulo" data-rule="minlen:5" data-msg="" />
                 
        </div>

<!--INPUT APELLIDO-->
        <p>Descripción</p>
         <div class="col-md-12 form-group">
            <textarea class="form-control" name="descripcion" id="descripcion" placeholder="Descripción" data-rule="minlen:15" data-msg=""></textarea> 
                
         </div>

<!--INPUT pregunta-->  
        <p>Ingrese su/s pregunta</p>
          <div class="col-md-12 form-group">
            <input type="text" name="pregunta" class="form-control" id="pregunta" placeholder="Pregunta" data-rule="minlen:5" data-msg="" />
                 
          </div>

<!--SUBMIT GUARDAR CAMBIOS -->      
          <div class="col-md-12 form-group">         
            <button type="submit"  class="get-started-btn">Agregar pregunta</button> 
          </div>

        </form> <!--CIERRA FORM?-->

<!--SUBMIT CANCELAR --> 
          <div class="col-md-6 form-group">            
            <a href="creartest.php"  class="get-started-btn">Cancelar</a> 
          </div>

<!--SUBMIT GUARDAR CAMBIOS -->      
          <div class="col-md-6 form-group">         
            a href="index.html"  class="get-started-btn">Finalizar</a> 
          </div>
            </form><!--CIERRA FORM?-->
 
      </center>
      </div>
      </div>
    </section>

<!--=============  End section About ======================================================== 
==========================================================================================-->


<!--=========================================================================================
=============  Section footer ============================================================-->

  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>EducApp</h3>
            <p>Buenos Aires, Argentina.</p>
              <p><strong>Email:</strong> educaapargentina@gmail.com</p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Links útiles</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Inicio</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Crear actividad</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Contactanos</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Preguntas frecuentes</a></li>

            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Otros links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">...</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">...</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">...</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Suscribite</h4>
            <p>Obtené todas las novedades</p>
            <form action="" method="post">
              <input type="email" name="text"><input type="submit" value="Suscribirse">
            </form>
          </div>

        </div>
      </div>
    </div>

    <div class="container d-md-flex py-4">

      <div class="mr-md-auto text-center text-md-left">
        <div class="copyright">
          &copy; Copyright <strong><span>VamNew</span></strong>. All Rights Reserved
        </div>
        <div class="credits">

          Designed by <a href="">BulacioMPaula</a>
        </div>
      </div>
      <div class="social-links text-center text-md-right pt-3 pt-md-0">
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div>
    </div>
  </footer>
<!--=============  End section Footer ======================================================= 
==========================================================================================-->

  <a href="#" class="back-to-top"><i class="bx bx-up-arrow-alt"></i></a>
  <div id="preloader"></div>
<!--=========================================================================================
=============  Vendor JS Files ============================================================--> 

  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>

  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

<!--=========================================================================================
============= Template Main JS File ======================================================--> 

  <script src="assets/js/main.js"></script>

</body>

</html>

<!-- ====================================           CHEQUEADO          ============================================== -->
<!-- ====================================          ¡ ATENCION !        ============================================== -->
<!-- ====================================                              ============================================== -->
<!-- ====================================           INCORPORAR         ============================================== -->
<!-- ====================================          Validaciones        ============================================== -->
<!-- ====================================              PHP             ============================================== -->
<!-- ====================================                              ============================================== -->
<!-- ====================================          ¡ ATENCION !        ============================================== -->
<!-- ====================================                              ============================================== -->
<!-- ====================================                              ============================================== -->
<!-- ====================================          ¡ ATENCION !        ============================================== -->

